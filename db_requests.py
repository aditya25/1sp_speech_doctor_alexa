import boto3
import collections
import json

class AWS_DB:
    client = ''
    all_question_groups = [];
    all_articulation_sounds = [];
    current_group_questions = [];
    current_group_max_age = 0;
    current_group_min_age = -1;
    all_fluency_techniques = [];

    def __init__(self):
        self.client = boto3.client('dynamodb',
                aws_access_key_id='AKIAJ36Y33WRHUVXLYRQ',
                aws_secret_access_key='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',
                region_name='ap-southeast-1'
        )

    def get_all_question_groups(self):
        response = self.client.scan(
            TableName='tbl_question_groups',
            Limit=123,
            Select='ALL_ATTRIBUTES'
        )

        for group in response['Items']:
            group['asked'] = False
            self.all_question_groups.append(group)

        return self.all_question_groups

    def get_question_group_by_position (self, groups, position, value, type):
        for question_group in groups:
            if question_group['position']['N'] == str(position):
                if value == "all":
                    return question_group
                else:
                    if type == 'string':
                        return question_group[value]['S']
                    if type == 'number':
                        return question_group[value]['N']
                    if type == 'none':
                        return question_group[value]
        return

    def get_questions_by_group (self, group_id):
        response = self.client.scan(
            TableName='tbl_questions',
            Limit=1000,
            Select='ALL_ATTRIBUTES',
            ScanFilter={
                'groupId': {
                    'AttributeValueList': [
                        {
                            'N': group_id,
                        },
                    ],
                    'ComparisonOperator': 'EQ'
                }
            }
        )
        # print response['Items']
        # Arrange in descending order of age
        response['Items'].sort(key=lambda t: int(t['subsetAge']['N']), reverse=True)
        self.current_group_questions = response['Items']
        self.current_group_max_age = int(response['Items'][0]['subsetAge']['N'])
        self.current_group_min_age = int(response['Items'][-1]['subsetAge']['N'])
        # print self.current_group_questions

    def get_current_group_max_age(self):
        return self.current_group_max_age

    def get_current_group_min_age(self):
        return self.current_group_min_age

    def get_questions_by_age (self, age, type):
        # print age
        if type == "lower" or type == "lower_equal":
            for question in self.current_group_questions:
                if age > int(question['subsetAge']['N']) and type == "lower":
                    nearest_age = int(question['subsetAge']['N'])
                    break
                if age >= int(question['subsetAge']['N']) and type == "lower_equal":
                    nearest_age = int(question['subsetAge']['N'])
                    break

        if type == "higher":
            for question in reversed(self.current_group_questions):
                if age < int(question['subsetAge']['N']) and type == "higher":
                    nearest_age = int(question['subsetAge']['N'])
                    break

        questions = []
        for question in self.current_group_questions:
            if nearest_age == int(question['subsetAge']['N']) :
                question['asked'] = False
                questions.append(question)
        return {'nearest_age' : nearest_age, 'questions' : questions}


    def get_combined_el_rl_result(self, rl_result, el_result):
        if rl_result == "red" and el_result == "red":
            return "Your child's expressive and receptive language skills are not developing as per his age. \
                    Your child needs help to enhance his speech and language skills. \
                    You must contact us at 1specialplace.com for a detailed evaluation to help us improve your child's Speech. \
                    Carry on using this app to keep an eye on how your child is progressing with his communication skills."

        if rl_result == "red" and el_result == "yellow":
            return "Your child's receptive language skills are not developing as per his age and his expressive language skills are slowly matching upto his peer level. \
                    You must contact us at 1specialplace.com for a detailed evaluation to help us improve your child's Speech. \
                    Carry on using this app to keep an eye on how your child is progressing with his communication."

        if rl_result == "red" and el_result == "green":
            return "Your child's expressive language skills are just as per his age but his receptive language skills are not developing as per his age. \
                    You must contact us at 1specialplace.com for a detailed evaluation to help us improve your child's Speech. \
                    Carry on using this app to keep an eye on how your child is progressing with his communication."

        if rl_result == "yellow" and el_result == "red":
            return "Your child's receptive language skills slowly matching upto his peer level but his expressive language skills are not developing as per his age. \
                    You must contact us at 1specialplace.com for a detailed evaluation to help us improve your child's Speech. \
                    Carry on using this app to keep an eye on how your child is progressing with his communication."

        if rl_result == "yellow" and el_result == "yellow":
            return "Your child's receptive and expressive language skills are slowly matching up to his peer level. \
                    Your child is progressing slowly to match the skills of his or her age group. \
                    We recommend that you start using some ideas on our website 1specialplace.com to boost his or her talking skills. \
                    Carry on using this app to keep an eye on how your child is progressing with his communication."

        if rl_result == "yellow" and el_result == "green":
            return "Your child's expressive language skills are just as per his age and his receptive language skills are slowly matching up to his peer level. \
                    Your child is progressing slowly to match the skills of his or her age group. \
                    We recommend that you start using some ideas on our website 1specialplace.com to boost his or her understanding skills. \
                    Carry on using this app to keep an eye on how your child is progressing with his communication."

        if rl_result == "green" and el_result == "red":
            return "Your child's receptive language skills are just as per his age but his expressive language skills are not developing as per his age. \
                    You must contact us at 1specilplace.com for a detailed evaluation to help us improve your child's Speech. \
                    Carry on using this app to keep an eye on how your child is progressing with his communication skills."

        if rl_result == "green" and el_result == "yellow":
            return "Your child's receptive language skills are just as per his age and his expressive language skills are slowly matching up to his peer level.  \
                    Your child is progressing slowly to match the skills of his or her age group. \
                    We recommend that you start using some ideas on our website 1specialplace.com to boost his or her talking skills. \
                    Carry on using this app to keep an eye on how your child is progressing with his communication."

        if rl_result == "green" and el_result == "green":
            return "Yay! Your child's receptive and expressive language skills are just as per his age.  \
                    This means things are on the right track for you and your little one! \
                    Try some ideas on our website 1specialplace.com to ensure that your child continues to progress well with his speaking and learning."

    def get_all_articulation_sounds(self):
        response = self.client.scan(
            TableName='tbl_articulation_sounds',
            Limit=123,
            Select='ALL_ATTRIBUTES'
        )
        self.all_articulation_sounds = response['Items']

    def get_articulation_sound_data_by_name(self, name):
        for sound in self.all_articulation_sounds:
            if sound['soundName']['S'] == name:
                return sound

    def get_articulation_sound_name(self, current_sound_data):
        return current_sound_data['soundName']['S']

    def get_articulation_sound_formation(self, current_sound_data):
        return current_sound_data['soundFormation']['S']

    def get_articulation_sound_age_range(self, current_sound_data):
        return {'min':int(current_sound_data['minAge']['N']), 'max':int(current_sound_data['maxAge']['N'])}

    def get_articulation_sound_words_by_position(self, current_sound_data, position):
        if position == "begin":
            words = json.loads(current_sound_data['beginWords']['S'])
        elif position == "middle":
            words = json.loads(current_sound_data['middleWords']['S'])
        elif position == "end":
            words = json.loads(current_sound_data['endWords']['S'])

        word_dict = []
        for word_name in words:
            word = {'name':word_name, 'asked':False}
            word_dict.append(word)

        return word_dict

    def get_articulation_sound_sentence(self, current_sound_data):

        word_dict = []
        for word_name in json.loads(current_sound_data['sentences']['S']):
            word = {'name':word_name, 'asked':False}
            word_dict.append(word)

        return word_dict

    def get_all_fluency_techniques(self):
        response = self.client.scan(
            TableName='tbl_fluency_techniques',
            Limit=123,
            Select='ALL_ATTRIBUTES'
        )

        response['Items'].sort(key=lambda t: int(t['position']['N']), reverse=False)

        for technique in response['Items']:
            technique['asked'] = False
            self.all_fluency_techniques.append(technique)

        return self.all_fluency_techniques

    def get_fluency_technique_by_id(self, id):
        for technique in self.all_fluency_techniques :
            if int(technique['fluencyTechniqueId']['N']) == id:
                return technique

    def get_fluency_technique_by_name(self, name):
        for technique in self.all_fluency_techniques :
            if technique['fluencyTechniqueName']['S'] == name:
                return technique

    def get_fluency_technique_all_sequences(self, technique):
        sequences = []
        for item in json.loads(technique['sequence']['S']):
            item['asked'] = False
            sequences.append(item)
        return sequences
