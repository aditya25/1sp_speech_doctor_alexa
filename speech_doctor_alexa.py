# coding=utf-8

# Speech Doctor
# By Aditya Agarwal <aditya@1specialplace.com>
#
# An alexa skill to assist in speech and language development

#TODO saying no when shall i continue in language

import logging
from datetime import datetime
from flask import Flask, json, render_template
from flask_ask import Ask, request, session, question, statement, delegate, audio
from dateutil import relativedelta
from db_requests import AWS_DB
import json

__author__ = 'Aditya Agarwal'
__email__ = 'aditya@1specialplace.com'

GROUP_POSITIONS = [2, 4]
VALID_SOUNDS = ['k', 'r']
VALID_POSITIONS = ['beginning', 'middle', 'end', 'all']
VALID_TECHNIQUES = ['breathing', 'pause', 'prolonging', 'beats']
app = Flask(__name__)
ask = Ask(app, '/')
logging.getLogger("flask_ask").setLevel(logging.DEBUG)

adb = AWS_DB()
# adb.get_all_fluency_techniques()
# print adb.get_fluency_technique_all_sequences(adb.get_fluency_technique_by_id(1))
# adb.get_all_articulation_sounds()
# sound = adb.get_articulation_sound_data_by_name('k')
# print adb.get_articulation_sound_sentence(sound)
# print adb.get_combined_el_rl_result("red", "red")
# group_id = adb.get_question_group_by_position(2, 'groupId', 'number')
# adb.get_questions_by_group(group_id)
# print json.dumps(adb.get_questions_by_age(40, "higher"), indent=2)
# Session starter
#
# This intent is fired automatically at the point of launch (= when the session starts).
# Use it to register a state machine for things you want to keep track of, such as what the last intent was, so as to be
# able to give contextual help.

def get_next_fluency_technique(technique_name=None):

    # Store the answer of previous question and mark it as asked
    if "current_fluency_technique_id" in session.attributes:
        count = 0
        for current in session.attributes["current_fluency_techniques"]:
            if int(current['fluencyTechniqueId']['N']) == session.attributes["current_fluency_technique_id"]:
                print "marking technique as asked"
                session.attributes["current_fluency_techniques"][count]['asked'] = True
                print json.dumps(current, indent=2)
            count = count + 1

    # Get all questions for this group and reset attributes
    technique = None
    for current in session.attributes["current_fluency_techniques"]:
        if current['asked'] == False:
            if technique_name != None:
                if technique_name == current['fluencyTechniqueName']['S']:
                    technique = current
                    break
            else:
                technique = current
                break

    if technique != None:
        print "returning next technique to be asked"
        session.attributes["current_fluency_technique_id"] = int(technique['fluencyTechniqueId']['N'])

        # Get questions specific to age
        data = adb.get_fluency_technique_all_sequences(technique)
        session.attributes["current_fluency_technique_sequences"] = data
        print json.dumps(technique, indent=2)

        statement_text = technique['introText']['S']
        return statement_text
    else:
        statement_text = render_template('fluency_invalid_technique')
        return statement_text

    # No groups left, tell final result
    statement_text = render_template('fluency_end')
    return statement_text


def get_next_fluency_technique_sequence(answer=None):

    # Mark previous sequence as asked
    if answer == True:
        if "current_fluency_technique_sequence_position" in session.attributes:
            count = 0
            for current in session.attributes["current_fluency_technique_sequences"]:
                if int(current['position']) == session.attributes["current_fluency_technique_sequence_position"]:
                    print "marking sequence asked"
                    print json.dumps(current, indent=2)
                    session.attributes["current_fluency_technique_sequences"][count]['asked'] = True
                count = count + 1

        # Return next sequence to be asked
        for current in session.attributes["current_fluency_technique_sequences"]:
            if current['asked'] == False:
                print "returning next sequence to be asked"
                print json.dumps(current, indent=2)
                session.attributes["current_fluency_technique_sequence_position"] = int(current['position'])
                statement_text = current['text']
                if current['sound'] == "":
                    return question(statement_text)
                else:
                    return audio(statement_text).play(current['sound'])

        # No sequence left, go to next technique
        statement_text = get_next_fluency_technique()
        # Return result
        return question(statement_text)
    else:
        farewell_text = render_template('cancel_bye')
        return statement(farewell_text)

## Articulation functions

def get_next_repeat_word(answer=None, applause=False, type="word"):

    if "current_word_repeat" in session.attributes:
        count = 0
        for current in session.attributes["current_word_repeat_data"]:
            if current['name'].lower() == session.attributes["current_word_repeat"].lower():
                print "marking repeat word asked"
                print json.dumps(current, indent=2)
                session.attributes["current_word_repeat_data"][count]['asked'] = True
            count = count + 1

    for word in session.attributes['current_word_repeat_data']:
        if word['asked'] == False:
            session.attributes['current_word_repeat'] = word['name']
            if applause == False:
                question_text = render_template('articulation_word_repeat',
                    word=word['name'],
                    type=type
                )
            else:
                question_text = render_template('articulation_word_repeat_applause',
                    word=word['name'],
                    type=type
                )
            return question(question_text).reprompt(question_text)

    return get_articulation_sequence_root(True)

def get_articulation_sequence_root(answer):
    if session.attributes['articulation_sequence_root'] == "articulation_sequence_start":
        session.attributes['articulation_sequence_root'] = "articulation_word_isolation"
        return get_next_articulation_question(answer)
    elif session.attributes['articulation_sequence_root'] == "articulation_word_isolation":
        session.attributes['articulation_sequence_root'] = "articulation_word_level"
        return get_next_articulation_question(answer)
    elif session.attributes['articulation_sequence_root'] == "articulation_word_level":
        session.attributes['articulation_sequence_root'] = "articulation_word_practice_begin"
        return get_next_articulation_question(answer)
    elif session.attributes['articulation_sequence_root'] == "articulation_word_practice_begin":
        session.attributes['articulation_sequence_root'] = "articulation_word_practice_started_begin"
        return get_next_articulation_question(answer)
    elif session.attributes['articulation_sequence_root'] == "articulation_word_practice_started_begin":
        session.attributes['articulation_sequence_root'] = "articulation_word_practice_middle"
        return get_next_articulation_question(answer)
    elif session.attributes['articulation_sequence_root'] == "articulation_word_practice_middle":
        session.attributes['articulation_sequence_root'] = "articulation_word_practice_started_middle"
        return get_next_articulation_question(answer)
    elif session.attributes['articulation_sequence_root'] == "articulation_word_practice_started_middle":
        session.attributes['articulation_sequence_root'] = "articulation_word_practice_end"
        return get_next_articulation_question(answer)
    elif session.attributes['articulation_sequence_root'] == "articulation_word_practice_end":
        session.attributes['articulation_sequence_root'] = "articulation_word_practice_started_end"
        return get_next_articulation_question(answer)
    elif session.attributes['articulation_sequence_root'] == "articulation_word_practice_started_end":
        session.attributes['articulation_sequence_root'] = "articulation_word_practice_sentences"
        return get_next_articulation_question(answer)
    elif session.attributes['articulation_sequence_root'] == "articulation_word_practice_sentences":
        session.attributes['articulation_sequence_root'] = "articulation_word_practice_started_sentence"
        return get_next_articulation_question(answer)
    elif session.attributes['articulation_sequence_root'] == "articulation_word_practice_started_sentence":
        session.attributes['articulation_sequence_root'] = "articulation_ended"
        return get_next_articulation_question(answer)

def get_next_articulation_question(answer):
    if answer == True:
        if session.attributes['articulation_sequence_root'] == "articulation_word_isolation":
            question_text = render_template('articulation_word_in_isolation_question',
                sound_name=adb.get_articulation_sound_name(session.attributes['current_sound_data']),
                sound_formation=adb.get_articulation_sound_formation(session.attributes['current_sound_data'])
            )
            return question(question_text).reprompt(question_text)
        elif session.attributes['articulation_sequence_root'] == "articulation_word_level":
            question_text = render_template('articulation_word_level_question',
                sound_name=adb.get_articulation_sound_name(session.attributes['current_sound_data'])
            )
            return question(question_text).reprompt(question_text)
        elif session.attributes['articulation_sequence_root'] == "articulation_word_practice_begin":
            question_text = render_template('articulation_word_practice_begin',
                sound_name=adb.get_articulation_sound_name(session.attributes['current_sound_data'])
            )
            session.attributes['current_word_repeat_position'] = "begin"
            session.attributes['current_word_repeat_data'] = adb.get_articulation_sound_words_by_position(session.attributes['current_sound_data'], "begin")
            return question(question_text).reprompt(question_text)
        elif session.attributes['articulation_sequence_root'] == "articulation_word_practice_middle":
            question_text = render_template('articulation_word_practice_middle',
                sound_name=adb.get_articulation_sound_name(session.attributes['current_sound_data'])
            )
            session.attributes['current_word_repeat_position'] = "middle"
            session.attributes['current_word_repeat_data'] = adb.get_articulation_sound_words_by_position(session.attributes['current_sound_data'], "middle")
            return question(question_text).reprompt(question_text)
        elif session.attributes['articulation_sequence_root'] == "articulation_word_practice_started_begin" or \
             session.attributes['articulation_sequence_root'] == "articulation_word_practice_started_middle" or \
             session.attributes['articulation_sequence_root'] == "articulation_word_practice_started_end":
            # No applause as this is the first time we ask to repeat, next is from YesIntent
            return get_next_repeat_word(True, False, "word")
        elif session.attributes['articulation_sequence_root'] == "articulation_word_practice_started_sentence":
            # No applause as this is the first time we ask to repeat, next is from YesIntent
            return get_next_repeat_word(True, False, "sentence")
        elif session.attributes['articulation_sequence_root'] == "articulation_word_practice_end":
            question_text = render_template('articulation_word_practice_end',
                sound_name=adb.get_articulation_sound_name(session.attributes['current_sound_data'])
            )
            session.attributes['current_word_repeat_position'] = "end"
            session.attributes['current_word_repeat_data'] = adb.get_articulation_sound_words_by_position(session.attributes['current_sound_data'], "end")
            return question(question_text).reprompt(question_text)
        elif session.attributes['articulation_sequence_root'] == "articulation_word_practice_sentences":
            question_text = render_template('articulation_word_practice_sentences',
                sound_name=adb.get_articulation_sound_name(session.attributes['current_sound_data'])
            )
            session.attributes['current_word_repeat_position'] = "sentence"
            session.attributes['current_word_repeat_data'] = adb.get_articulation_sound_sentence(session.attributes['current_sound_data'])
            return question(question_text)
        elif session.attributes['articulation_sequence_root'] == "articulation_ended":
            question_text = render_template('articulation_word_applause')
            return statement(question_text)
    elif answer == False:
        farewell_text = render_template('cancel_bye')
        return statement(farewell_text)

def get_next_group(result=None):

    # Store the answer of previous question and mark it as asked
    if "current_group_id" in session.attributes:
        count = 0
        for current_group in session.attributes["current_groups"]:
            if int(current_group['groupId']['N']) == session.attributes["current_group_id"]:
                print "recording result for group"
                session.attributes["current_groups"][count]['asked'] = True
                session.attributes["current_groups"][count]['result'] = result
                print json.dumps(current_group, indent=2)
            count = count + 1

    # Get all questions for this group and reset attributes
    for current_group in session.attributes["current_groups"]:
        if current_group['asked'] == False and int(current_group['position']['N']) in GROUP_POSITIONS:
            print "returning next group to be asked"
            session.attributes["current_group_id"] = int(current_group['groupId']['N'])

            # Get questions for that group
            adb.get_questions_by_group(str(session.attributes["current_group_id"]))

            # Get questions specific to age
            data = adb.get_questions_by_age(session.attributes["child_age"], "lower_equal")
            session.attributes["current_questions"] = data['questions']
            session.attributes["current_group_age"] = data['nearest_age']
            session.attributes["current_direction"] = "initial"
            print json.dumps(current_group, indent=2)

            # if result == None:
                # statement_text = render_template('quiz_start') + current_group['introText']['S'] + '. Shall I continue? Please say yes or no'
            # else:
            # statement_text = current_group['introText']['S'] + '. Shall I continue? Please say yes or no'
            statement_text = current_group['alexaIntroText']['S']
            return question(statement_text)

    # No groups left, tell final result
    statement_text = "Thank you for answering expressive language questions. " + adb.get_combined_el_rl_result(
        adb.get_question_group_by_position(session.attributes["current_groups"], 2, 'result', 'none'),
        adb.get_question_group_by_position(session.attributes["current_groups"], 4, 'result', 'none')
    ) + " " + render_template('cancel_bye')
    return statement(statement_text)


def get_group_result():
    age_difference = session.attributes["current_group_age"] - session.attributes["child_age"]
    lower_than_current_group_age = adb.get_questions_by_age(session.attributes['current_group_age'], "lower")
    yellow_range = session.attributes["current_group_age"] - lower_than_current_group_age['nearest_age']
    if age_difference >= 0:
        return "green"
    elif age_difference < 0 and abs(age_difference) < yellow_range:
        return "yellow"
    elif age_difference < 0:
        return "red"

def check_max_age_before_continue(direction):
    if session.attributes["current_group_age"] >= adb.get_current_group_max_age():
        print "max age detected " +  str(adb.get_current_group_max_age())
        return "stop"
    elif session.attributes["current_group_age"] <= adb.get_current_group_min_age():
        print "min age detected " + str(adb.get_current_group_min_age())
        return "stop"
    else:
        return direction

def get_direction():
    truecount = 0
    for current_question in session.attributes["current_questions"]:
        if current_question['answer'] == True:
            truecount = truecount + 1

    print "number of questions is " + str(len(session.attributes["current_questions"]))
    print "truecount is " + str(truecount)

    direction = ""
    if len(session.attributes["current_questions"]) == 3:
        if truecount < 2:
            direction = "backward"
        if truecount == 2:
            direction = "stop"
        if truecount == 3:
            direction = "forward"

    if len(session.attributes["current_questions"]) == 4:
        if truecount < 3:
            direction = "backward"
        if truecount == 3:
            direction = "stop"
        if truecount == 4:
            direction = "forward"

    if len(session.attributes["current_questions"]) == 5:
        if truecount < 3:
            direction = "backward"
        if truecount == 3 or truecount == 4:
            direction = "stop"
        if truecount == 5:
            direction = "forward"

    if len(session.attributes["current_questions"]) == 6:
        if truecount < 4:
            direction = "backward"
        if truecount == 4 or truecount == 5:
            direction = "stop"
        if truecount == 6:
            direction = "forward";

    if session.attributes["current_direction"] != "initial" :
        if direction != session.attributes["current_direction"]:
            # If direction changes from back to forw then stop
            # If direction changes to stop from forw/back then stop
            # If direction changes from forw to back then stop
            print "direction change detected"
            return "stop"
        else:
            # Keep going forward if previously going forward
            # Keep going backward if previously going backward
            print "direction same detected"
            return check_max_age_before_continue(direction)

    else:
        print "initial direction detected"
        return check_max_age_before_continue(direction)

def get_next_question(answer=None):

    # Store the answer of previous question and mark it as asked
    if "current_question_id" in session.attributes:
        count = 0
        for current_question in session.attributes["current_questions"]:
            if int(current_question['questionId']['N']) == session.attributes["current_question_id"]:
                print "recording answer for question"
                print json.dumps(current_question, indent=2)
                session.attributes["current_questions"][count]['asked'] = True
                session.attributes["current_questions"][count]['answer'] = answer
            count = count + 1

    # Return next question to be asked
    for current_question in session.attributes["current_questions"]:
        if current_question['asked'] == False:
            print "returning next question to be asked"
            print json.dumps(current_question, indent=2)
            session.attributes["current_question_id"] = int(current_question['questionId']['N'])
            statement_text = "Your child " + current_question['text']['S']
            return question(statement_text)

    # No questions left, move forward, backward or stop
    session.attributes['current_direction'] = get_direction()
    print "direction " + session.attributes['current_direction']

    if session.attributes['current_direction'] == "forward":
        data = adb.get_questions_by_age(session.attributes["current_group_age"], "higher")
        print "direction - forward, getting new question data for higher age"
        print json.dumps(data, indent=2)
        session.attributes["current_questions"] = data['questions']
        session.attributes["current_group_age"] = data['nearest_age']

        # Return question from new set
        return get_next_question(True)

    elif session.attributes['current_direction'] == "backward":
        session.attributes['current_direction'] =  get_direction()
        data = adb.get_questions_by_age(session.attributes["current_group_age"], "lower")
        print "direction - backward, getting new question data for lower age"
        print json.dumps(data, indent=2)
        session.attributes["current_questions"] = data['questions']
        session.attributes["current_group_age"] = data['nearest_age']

        # Return question from new set
        return get_next_question(True)

    elif session.attributes['current_direction'] == "stop":
        result = get_group_result()
        print "getting result " + result

        # statement_text = get_next_group(result)

        # Return result
        return get_next_group(result)

@ask.on_session_started
def start_session():
    """
    Fired at the start of the session, this is a great place to initialise state variables and the like.
    """
    # print get_next_group()
    logging.debug("Session started at {}".format(datetime.now().isoformat()))

# Launch intent
#
# This intent is fired automatically at the point of launch.
# Use it as a way to introduce your Skill and say hello to the user. If you envisage your Skill to work using the
# one-shot paradigm (i.e. the invocation statement contains all the parameters that are required for returning the
# result

@ask.launch
def handle_launch():
    """
    (QUESTION) Responds to the launch of the Skill with a welcome statement and a card.

    Templates:
    * Initial statement: 'welcome'
    * Reprompt statement: 'welcome_re'
    * Card title: 'Speech Doctor
    * Card body: 'welcome_card'
    """

    welcome_text = render_template('welcome')
    welcome_re_text = render_template('welcome_re')
    welcome_card_text = render_template('welcome_card')

    return question(welcome_text).reprompt(welcome_re_text).standard_card(title="Speech Doctor",
                                                                          text=welcome_card_text)


# Built-in intents
#
# These intents are built-in intents. Conveniently, built-in intents do not need you to define utterances, so you can
# use them straight out of the box. Depending on whether you wish to implement these in your application, you may keep
# or delete them/comment them out.
#
# More about built-in intents: http://d.pr/KKyx

@ask.intent('AMAZON.StopIntent')
def handle_stop():
    """
    (STATEMENT) Handles the 'stop' built-in intention.
    """
    farewell_text = render_template('stop_bye')
    return statement(farewell_text)


@ask.intent('AMAZON.CancelIntent')
def handle_cancel():
    """
    (STATEMENT) Handles the 'cancel' built-in intention.
    """
    farewell_text = render_template('cancel_bye')
    return statement(farewell_text)


@ask.intent('AMAZON.HelpIntent')
def handle_help():
    """
    (QUESTION) Handles the 'help' built-in intention.

    You can provide context-specific help here by rendering templates conditional on the help referrer.
    """

    help_text = render_template('help_text')
    return question(help_text)


@ask.intent('AMAZON.NoIntent')
def handle_no():
    """
    (?) Handles the 'no' built-in intention.
    """
    if "root" in session.attributes:
        if session.attributes['root'] == "language_quiz_started":
            return get_next_question(False)
        else:
            farewell_text = render_template('cancel_bye')
            return statement(farewell_text)
    else:
        farewell_text = render_template('invalid_intent')
        return statement(farewell_text)


@ask.intent('AMAZON.YesIntent')
def handle_yes():
    """
    (?) Handles the 'yes'  built-in intention.
    """
    # pass
    # print session.attributes['current_questions']
    if "root" in session.attributes:
        if session.attributes['root'] == "fluency":
            # Coming from unimplemented modes, ask for dob and set language mode
            # question_text = render_template('dob_question')
            # session.attributes['root'] = "language_quiz_started"
            # return question(question_text).reprompt(question_text)
            session.attributes['current_fluency_techniques'] = adb.get_all_fluency_techniques()
            session.attributes['root'] = "fluency_started"
            question_text =  get_next_fluency_technique()
            return question(question_text)
        elif session.attributes['root'] == "fluency_started":
            return get_next_fluency_technique_sequence(True)
        # Start asking questions if coming from language intent or continue asking them if quiz already started
        elif session.attributes['root'] == "language" or session.attributes['root'] == "language_quiz_started":
            session.attributes['root'] = "language_quiz_started"
            return get_next_question(True)
        elif session.attributes['root'] == "articulation_within_age":
            if 'articulation_sequence_root' not in session.attributes:
                session.attributes['articulation_sequence_root'] = "articulation_sequence_start"
            return get_articulation_sequence_root(True)
        else:
            print "didnt match any valid roots in yes intent"
            farewell_text = render_template('cancel_bye')
            return statement(farewell_text)
    else:
        print "no root in yes intent"
        farewell_text = render_template('invalid_intent')
        return statement(farewell_text)


@ask.intent('AMAZON.NextIntent')
def handle_back():
    """
    (?) Handles the 'go back!'  built-in intention.
    """
    if "root" in session.attributes:
        if session.attributes['root'] == "articulation_within_age":
            if 'articulation_sequence_root' not in session.attributes:
                session.attributes['articulation_sequence_root'] = "articulation_sequence_start"
            return get_articulation_sequence_root(True)
        elif session.attributes['root'] == "fluency_started":
            return get_next_fluency_technique_sequence(True)
        else:
            print "didnt match any valid roots in next intent"
            farewell_text = render_template('cancel_bye')
            return statement(farewell_text)
    else:
        print "no root in yes intent"
        farewell_text = render_template('invalid_intent')
        return statement(farewell_text)

@ask.intent('AMAZON.PreviousIntent')
def handle_back():
    """
    (?) Handles the 'go back!'  built-in intention.
    """
    pass

@ask.intent('AMAZON.StartOverIntent')
def start_over():
    """
    (QUESTION) Handles the 'start over!'  built-in intention.
    """
    pass


# Ending session
#
# This intention ends the session.

@ask.session_ended
def session_ended():
    """
    Returns an empty for `session_ended`.

    .. warning::

    The status of this is somewhat controversial. The `official documentation`_ states that you cannot return a response
    to ``SessionEndedRequest``. However, if it only returns a ``200/OK``, the quit utterance (which is a default test
    utterance!) will return an error and the skill will not validate.

    """
    return statement("")

def get_age_in_months(dob):
    DATE_FORMATS = ['%Y-%m-%d', '%Y-%m']
    for date_format in DATE_FORMATS:
        try:
            my_date = datetime.strptime(dob, date_format)
            r = relativedelta.relativedelta(datetime.now(), my_date)
            age = r.years * 12 + r.months
        except ValueError:
            pass
        else:
          return age
    return None

@ask.intent('LanguageIntent', mapping={'dob':'DOB'})
def handle_language(dob):
    if session.get('dialogState', '') != "COMPLETED":
        return delegate()

    session.attributes['root'] = 'language';
    session.attributes["child_dob"] = dob
    try :
        # r = relativedelta.relativedelta(datetime.now(), datetime.strptime(dob, "%Y-%m-%d"))
        # Get child age in month
        session.attributes["child_age"] =  get_age_in_months(dob);
        if session.attributes["child_age"] <= 120 and session.attributes["child_age"] > 0:
            # Within age range supported by the app
            session.attributes['current_groups'] = adb.get_all_question_groups()
            return get_next_group()
        else:
            print "max age violation DOB"
            reprompt_text = render_template('max_age')
            return statement(reprompt_text)
    except ValueError:
        print "value error DOB"
        reprompt_text = render_template('invalid_date')
        return statement(reprompt_text)
    # question_text = render_template('dob_question')
    # reprompt_text = render_template('dob_question')
    # return question(question_text).reprompt(reprompt_text)


@ask.intent('ArticulationIntent', mapping={'sound_name':'SOUND_NAME', 'dob':'DOB'})
def handle_articulation(sound_name, dob):
    if session.get('dialogState', '') != "COMPLETED":
        return delegate()
    session.attributes['root'] = 'articulation';
    print "got sound name " + sound_name
    if sound_name in VALID_SOUNDS:
        session.attributes['root'] = 'articulation_sound_name';
        adb.get_all_articulation_sounds()
        session.attributes['current_sound_data'] = adb.get_articulation_sound_data_by_name(sound_name)
        session.attributes["child_dob"] = dob
        try :
            # r = relativedelta.relativedelta(datetime.now(), datetime.strptime(dob, "%Y-%m-%d"))
            # Get child age in month
            session.attributes["child_age"] =  get_age_in_months(dob);
            if session.attributes["child_age"] <= 120 and session.attributes["child_age"] > 0:
                # Within age range supported by the app
                sound_age_range = adb.get_articulation_sound_age_range(session.attributes['current_sound_data'])
                print "getting sound range"
                print sound_age_range
                if session.attributes["child_age"] >= sound_age_range['max'] * 12:
                # if session.attributes["child_age"] <= sound_age_range['max'] * 12 and session.attributes["child_age"] >= 12 * sound_age_range['min']:
                    session.attributes["root"] = "articulation_within_age"
                    print "age range is concerning for this sound"
                    question_text = render_template('articulation_within_age_question',
                        child_age=session.attributes["child_age"]/12,
                        sound_name=adb.get_articulation_sound_name(session.attributes["current_sound_data"])
                    )
                    return question(question_text).reprompt(question_text)
                else:
                    print "out of concern age range for this sound"
                    statement_text = render_template('articulation_below_age',
                        sound_name=adb.get_articulation_sound_name(session.attributes["current_sound_data"]),
                        sound_age_range_min=sound_age_range['min'],
                        sound_age_range_max=sound_age_range['max']
                    )
                    return statement(statement_text)
            else:
                print "max age violation DOB"
                reprompt_text = render_template('max_age')
                return statement(reprompt_text)
        except ValueError:
            print "value error DOB"
            reprompt_text = render_template('invalid_date')
            return question(reprompt_text)
        # question_text = render_template('dob_question')
        # return question(question_text).reprompt(question_text)
    else:
        statement_text = render_template('articulation_sound_not_available')
        return statement(statement_text)
    # question_text = render_template('articulation_question')
    # reprompt_text = render_template('articulation_reprompt_question')
    return question(question_text).reprompt(reprompt_text)


@ask.intent('SoundSentencePracticeIntent', mapping={'sound_name':'SOUND_NAME'})
def handle_articulation_sound_sentence_practice(sound_name):
    if session.get('dialogState', '') != "COMPLETED":
        return delegate()
    if sound_name in VALID_SOUNDS:
        print "practice senteces for " + sound_name
        session.attributes['root'] = 'articulation_within_age'
        session.attributes['articulation_sequence_root'] = "articulation_word_practice_sentences"
        adb.get_all_articulation_sounds()
        session.attributes['current_sound_data'] = adb.get_articulation_sound_data_by_name(sound_name)
        return get_next_articulation_question(True)
    else:
        statement_text = render_template('articulation_sound_not_available')
        return statement(statement_text)

@ask.intent('SoundWordPracticeIntent', mapping={'sound_name':'SOUND_NAME', 'sound_position':'SOUND_POSITION'})
def handle_articulation_sound_word_practice(sound_name, sound_position):
    if session.get('dialogState', '') != "COMPLETED":
        return delegate()
    print "practice words for " + sound_name
    if sound_name in VALID_SOUNDS and sound_position in VALID_POSITIONS:
        print "practice words for " + sound_name
        session.attributes['root'] = 'articulation_within_age'
        adb.get_all_articulation_sounds()
        session.attributes['current_sound_data'] = adb.get_articulation_sound_data_by_name(sound_name)
        print "current sound"
        print session.attributes['current_sound_data']
        if sound_position == "all":
            session.attributes['articulation_sequence_root'] = "articulation_word_practice_begin"
        else:
            if sound_position == "beginning":
                session.attributes['articulation_sequence_root'] = "articulation_word_practice_begin"
            elif sound_position == "middle":
                session.attributes['articulation_sequence_root'] = "articulation_word_practice_middle"
            elif sound_position == "end":
                session.attributes['articulation_sequence_root'] = "articulation_word_practice_end"
        return get_next_articulation_question(True)
    else:
        statement_text = render_template('articulation_sound_not_available')
        return statement(statement_text)


@ask.intent('FluencyIntent')
def handle_fluency():
    session.attributes['root'] = 'fluency';
    question_text = render_template('fluency_intro_question')
    adb.get_all_fluency_techniques()
    return question(question_text).reprompt(question_text)

@ask.on_playback_nearly_finished()
def nearly_finished():
    print "playback nearly finished"
    msg = 'playback nearly finished'
    return audio(msg).stop().simple_card(msg)

@ask.on_playback_finished()
def play_back_finished():
    print "playback finished"
    # if session.attributes['root'] == "fluency_started":
        # return get_next_fluency_technique_sequence(True)
    # else:
    farewell_text = render_template('cancel_bye')
    return statement(farewell_text)

@ask.intent('FluencyExercisesIntent', mapping={'fluency_exercise':'FLUENCY_EXERCISE'})
def handle_fluency_exercises_intent(fluency_exercise):
    if session.get('dialogState', '') != "COMPLETED":
        return delegate()
    if fluency_exercise in VALID_TECHNIQUES:
        session.attributes['root'] = 'fluency';
        session.attributes['current_fluency_techniques'] = adb.get_all_fluency_techniques()
        session.attributes['root'] = "fluency_started"
        question_text =  get_next_fluency_technique(fluency_exercise)
        return question(question_text)
    else:
        statement_text = render_template('fluency_invalid_technique')
        return statement(statement_text)

@ask.intent('SoundWordRepeateIntent', mapping={'sound_word_name':'SOUND_WORD_NAME'})
def handle_fluency_exercises_intent(sound_word_name):
    if session.get('dialogState', '') != "COMPLETED":
        return delegate()
    if sound_word_name.lower() ==  session.attributes['current_word_repeat'].lower():
        return get_next_repeat_word(True, True, "word")
    else:
        statement_text = render_template('articulation_word_repeat_error')
        return question(statement_text)

@ask.intent('SoundSentenceRepeatIntent', mapping={'sound_word_name':'SOUND_WORD_NAME'})
def handle_fluency_exercises_intent(sound_word_name):
    if session.get('dialogState', '') != "COMPLETED":
        return delegate()
    if sound_word_name.lower() in session.attributes['current_word_repeat'].lower():
        return get_next_repeat_word(True, True, "sentence")
    else:
        statement_text = render_template('articulation_word_repeat_error')
        return question(statement_text)


if __name__ == '__main__':
    app.run(debug=True)
